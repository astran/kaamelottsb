package com.acecorp.kaamelottsb

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import com.acecorp.kaamelottsb.services.PlayQuoteService
import com.acecorp.kaamelottsb.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_main.progressBar
import kotlinx.android.synthetic.main.activity_tabbed_quote_list.*

class TabbedQuoteList : AppCompatActivity(), OnListQuoteFragmentInteractionListner, ActivityWithProgressBar {

    var currentSearch: String = ""
    var currentQuoteCategory: QuoteCategory = QuoteCategory.getByTabIndex(0)
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu?.findItem(R.id.quote_search)
        val searchText = searchItem?.actionView as SearchView
        searchText.imeOptions = EditorInfo.IME_ACTION_DONE
        searchText.isIconified = false
        searchText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                currentSearch = newText ?: ""
                updateFilter()
                return false
            }

        })

        return true
    }

    fun updateFilter() {
        val viewPager = view_pager
        filter(viewPager.currentItem, currentSearch)
    }

    fun updateFilter(tabId: Int) {
        filter(tabId, currentSearch)
    }


    private fun filter(tabId: Int, string: String?) {
        val viewPager = view_pager
        val sectionsPagerAdapter = viewPager.adapter as SectionsPagerAdapter
        sectionsPagerAdapter.filter(tabId, string ?: "")
    }

    override fun progressValue(): ProgressValue {
        return ProgressBarValue(progressBar)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.reload_quotes -> {
                reloadQuotes()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun reloadQuotes() {

        currentQuoteCategory.quoteLoaderFunction(this, true) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_tabbed_quote_list)

        setSupportActionBar(tabToolbar)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabSelected(tab)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                tabSelected(tab)
            }
        })
    }

    private fun tabSelected(tab: TabLayout.Tab?) {
        currentQuoteCategory = QuoteCategory.getByTabIndex(tab?.position ?: 0)
        updateFilter(tab?.position ?: 0)
    }

    override fun onListFragmentInteraction(item: Quote?) {
        PlayQuoteService.publishIntent(this, item!!.fileRef.absolutePath)
    }
}