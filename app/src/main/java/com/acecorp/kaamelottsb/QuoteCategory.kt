package com.acecorp.kaamelottsb

import android.content.Context
import com.acecorp.kaamelottsb.services.data.QuoteService
import com.acecorp.kaamelottsb.services.data.QuotesLoader
import kotlin.reflect.KFunction3

enum class QuoteCategory(
    val tabIndex: Int,
    val displayNameResourceId: Int,
    val downloadUrl: String,
    val zipFileName: String,
    val directoryName: String,
    val quoteLoaderFunction: KFunction3<Context, Boolean, (List<Quote>) -> Unit, Unit>
) {
    ALL(0, R.string.tab_all, "na", "na", "na", QuoteService::loadAllQuotes),
    DIV(
        1,
        R.string.tab_div,
        "https://gitlab.com/astran/sb-sounds/-/jobs/artifacts/master/raw/div.zip?job=create_zip",
        "div.zip",
        "div",
        QuoteService::loadDiv
    ),
    KAAMELOTT(
        2, R.string.tab_kaamelott,
        "https://gitlab.com/astran/sb-sounds/-/jobs/artifacts/master/raw/kaamelott.zip?job=create_zip",
        "kaamelott.zip",
        "kaamelott",
        QuoteService::loadKaamelott
    );

    companion object {
        public fun getByTabIndex(tabIndex: Int): QuoteCategory {
            return values().first { it.tabIndex == tabIndex }
        }
    }


}