package com.acecorp.kaamelottsb

interface ActivityWithProgressBar {
    fun progressValue():ProgressValue
}