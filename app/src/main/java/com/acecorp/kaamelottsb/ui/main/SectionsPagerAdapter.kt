package com.acecorp.kaamelottsb.ui.main

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.acecorp.kaamelottsb.QuoteCategory
import com.acecorp.kaamelottsb.QuoteFragment


/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {
    private val tabs: MutableMap<Int, QuoteFragment> = mutableMapOf()
    override fun getItem(position: Int): Fragment {
        val quoteFragment = QuoteFragment.newInstance(QuoteCategory.getByTabIndex(position))
        tabs[position] = quoteFragment
        return quoteFragment
    }

    fun filter(tab: Int, s: CharSequence) {
        tabs[tab]?.filter(s)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(QuoteCategory.getByTabIndex(position).displayNameResourceId)
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 3
    }
}