package com.acecorp.kaamelottsb.services.data

import android.content.Context
import com.acecorp.kaamelottsb.ActivityWithProgressBar
import com.acecorp.kaamelottsb.Quote
import com.acecorp.kaamelottsb.QuoteCategory
import com.acecorp.kaamelottsb.services.StarsService
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.InputStreamReader

object QuotesLoader {

    fun loadDiv(context: Context, forceDownload: Boolean = false, callbacks: (List<Quote>) -> Unit) {
        loadQuoteCategory(
            context,
            forceDownload,
            QuoteCategory.DIV
        ) { divQuotes ->
            callbacks(divQuotes)
        }
    }

    fun loadKaamelott(context: Context, forceDownload: Boolean = false, callbacks: (List<Quote>) -> Unit) {
        loadQuoteCategory(
            context,
            forceDownload,
            QuoteCategory.KAAMELOTT
        ) { kaamelottQuotes ->
            callbacks(kaamelottQuotes)
        }


    }


    private fun loadQuoteCategory(
        context: Context,
        forceDownload: Boolean,
        quoteCategory: QuoteCategory,
        callback: (List<Quote>) -> Unit
    ) {
        val filesDir = context.filesDir
        StarsService.init(filesDir)
        val soundDirs = File(filesDir, "sounds")
        val downloadDir = File(soundDirs, quoteCategory.directoryName)
        loadZip(
            downloadDir,
            context,
            forceDownload,
            quoteCategory.zipFileName,
            quoteCategory.downloadUrl

        ) { quotes ->
            val quotesWithCategory =
                quotes.map { it.copy(quoteCategory = quoteCategory) }
            callback(quotesWithCategory)
        }
    }

    private fun loadZip(
        divDir: File,
        context: Context,
        forceDownload: Boolean,
        zipName: String,
        zipUrl: String,
        callback: (List<Quote>) -> Unit

    ) {
        if (forceDownload) {
            val mainActivity = context as ActivityWithProgressBar
            QuoteDownloader.reload(
                zipName,
                zipUrl, context, mainActivity.progressValue()
            ) {
                val divQuotes = parseSoundFile(divDir)
                callback(divQuotes)
            }
        } else {
            callback(parseSoundFile(divDir))
        }
    }

    private fun parseSoundFile(directory: File): List<Quote> {
        if (!directory.exists()) {
            return emptyList()
        }
        val divSoundInputStream = FileInputStream(File(directory, "sounds.json"))
        val divQuotes = loadQuotesFromFile(divSoundInputStream)!!.map { it.copy(fileRef = File(directory, it.file)) }
        return divQuotes
    }

    private fun loadQuotesFromFile(inputStream: InputStream): Array<Quote>? {
        val gson = Gson()
        val inputStreamReader = InputStreamReader(inputStream)
        val quotes = gson.fromJson(inputStreamReader, Array<Quote>::class.java)
        inputStreamReader.close()
        return quotes
    }
}