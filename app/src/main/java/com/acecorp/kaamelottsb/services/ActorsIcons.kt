package com.acecorp.kaamelottsb.services

import com.acecorp.kaamelottsb.R

object ActorsIcons {
    private val actorsIcons = mapOf(
        "Arthur - Le Roi Burgonde" to R.drawable.roi_burgonde,
        "Le Maître d'armes" to R.drawable.maitre_armes,
        "Merlin" to R.drawable.merlin,
        "Arthur" to R.drawable.arthur,
        "Breccan" to R.drawable.breccan,
        "Léodagan" to R.drawable.leodagan,
        "Attila" to R.drawable.attila,
        "Guenièvre" to R.drawable.guenievre,
        "Bohort" to R.drawable.bohort,
        "Le Répurgateur" to R.drawable.repurgateur,
        "Venec" to R.drawable.venec,
        "Le tavernier" to R.drawable.tavernier,
        "L'Interprète" to R.drawable.interprete,
        "Perceval" to R.drawable.perceval,
        "Dagonet" to R.drawable.dagonet,
        "Lancelot" to R.drawable.lancelot,
        "Karadoc" to R.drawable.karadoc,
        "Angharad" to R.drawable.angharad,
        "Demetra" to R.drawable.demetra,
        "L'évêque Boniface" to R.drawable.boniface,
        "La Dame du Lac" to R.drawable.dame_du_lac,
        "Yvain" to R.drawable.yvain,
        "Arthur - Galessin" to R.drawable.galessin,
        "Calogrenant" to R.drawable.calogrenant,
        "Roparzh" to R.drawable.roparzh,
        "Dame Séli" to R.drawable.dame_seli,
        "Kadoc" to R.drawable.kadoc,
        "Élias" to R.drawable.elias,
        "Caius" to R.drawable.caius,
        "Guethenoc" to R.drawable.guethenoc,
        "Elias de Kelliwic'h" to R.drawable.elias,
        "Le Roi Burgonde" to R.drawable.roi_burgonde,
        "Loth" to R.drawable.loth,
        "Kay" to R.drawable.kay,
        "Titus Nipius Glaucia" to R.drawable.titus_nipius_glaucia,
        "Goustan" to R.drawable.goustan,
        "Père Blaise" to R.drawable.pere_blaise,
        "Jacca" to R.drawable.jacca,
        "Grüdü" to R.drawable.grudu,
        "Anna" to R.drawable.anna,
        "Appius Manilius" to R.drawable.appius_manilius,
        "Spurius Cordius Frontiniu" to R.drawable.spurius_cordius_frontinius,
        "Le Barde" to R.drawable.barde,
        "Azénor" to R.drawable.azenor,
        "Gauvain" to R.drawable.gauvain,
        "Narces" to R.drawable.narces,
        "Belt" to R.drawable.belt,
        "Galessin" to R.drawable.galessin,
        "Fearmac" to R.drawable.fearmac,
        "Mévanoui" to R.drawable.mevanwi,
        "Hervé De Rinel" to R.drawable.rinel,
        "Urgan" to R.drawable.urgan,
        "Séli" to R.drawable.dame_seli,
        "La Duchesse d'Aquitaine" to R.drawable.duchesse_aquitaine,
        "Le Duc d'Aquitaine" to R.drawable.duc_aquitaine,
        "1Meme" to R.drawable.noun_derp_105262,
        "Meme" to R.drawable.noun_derp_105262
    )

    fun retrieveActorIcon(actor: String): Int {
        return actorsIcons.get(actor) ?: R.drawable.noun_derp_105262
    }


}