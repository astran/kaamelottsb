package com.acecorp.kaamelottsb.services

import android.os.AsyncTask
import android.os.PowerManager.WakeLock
import com.acecorp.kaamelottsb.ProgressValue
import java.io.*
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class DownloadTask(private val downloadFile: File,private val progressBar: ProgressValue, val callback:(file:File)->Unit) : AsyncTask<String?, Int?, String?>() {
    private val mWakeLock: WakeLock? = null

    override fun onPreExecute() {
        super.onPreExecute()
        progressBar.reset()
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)
        progressBar.progress(values[0]?:1)
    }

    override fun onPostExecute(s: String?) {
        progressBar.hide()
        super.onPostExecute(s)
        callback(downloadFile)
    }

    override fun doInBackground(vararg sUrl: String?): String? {
        var input: InputStream? = null
        var output: OutputStream? = null
        var connection: HttpURLConnection? = null
        try {
            val url = URL(sUrl[0])
            connection = url.openConnection() as HttpURLConnection
            connection.connect()

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection!!.responseCode != HttpURLConnection.HTTP_OK) {
                return ("Server returned HTTP " + connection.responseCode
                        + " " + connection.responseMessage)
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            val fileLength = connection.contentLength

            // download the file
            input = connection.inputStream

            output = FileOutputStream(downloadFile)
            val data = ByteArray(4096)
            var total: Long = 0
            var count: Int
            while (input.read(data).also { count = it } != -1) {
                // allow canceling with back button
                if (isCancelled) {
                    input.close()
                    return null
                }
                total += count.toLong()
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((total * 100 / fileLength).toInt())
                output.write(data, 0, count)
            }
        } catch (e: Exception) {
            throw e
        } finally {
            try {
                output?.close()
                input?.close()
            } catch (ignored: IOException) {
            }
            connection?.disconnect()
        }
        return null
    }

}