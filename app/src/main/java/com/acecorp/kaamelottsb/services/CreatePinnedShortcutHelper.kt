package com.acecorp.kaamelottsb.services

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.widget.Toast
import com.acecorp.kaamelottsb.PlayQuoteActivity
import com.acecorp.kaamelottsb.Quote

object CreatePinnedShortcutHelper {

    fun pinShortcut(context: Context, item: Quote) {
        val shortcutManager = context.getSystemService(ShortcutManager::class.java)

        if (shortcutManager!!.isRequestPinShortcutSupported) {
            // Assumes there's already a shortcut with the ID "my-shortcut".
            // The shortcut must be enabled.
            val i = Intent(context, PlayQuoteActivity::class.java)

            i.putExtra("file", item.fileRef.absolutePath)
            i.action = "PLAY_QUOTE"
            val shortcutIcon = ActorsIcons.retrieveActorIcon(item.character)
            val shortLable = if (item.title.length > 20) item.title.subSequence(0, 20) else item.title
            val shortcutInfo = ShortcutInfo
                .Builder(context, item.fileRef.absolutePath)

                .setShortLabel(shortLable)
                .setLongLabel(item.title)
                .setIntent(i)
            if (shortcutIcon != 0) {
                shortcutInfo.setIcon(Icon.createWithResource(context, shortcutIcon))
            }
            val pinShortcutInfo = shortcutInfo
                .build()

            // Create the PendingIntent object only if your app needs to be notified
            // that the user allowed the shortcut to be pinned. Note that, if the
            // pinning operation fails, your app isn't notified. We assume here that the
            // app has implemented a method called createShortcutResultIntent() that
            // returns a broadcast intent.
            val pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(pinShortcutInfo)

            // Configure the intent so that your app's broadcast receiver gets
            // the callback successfully.For details, see PendingIntent.getBroadcast().
            val successCallback = PendingIntent.getBroadcast(
                context, /* request code */ 0,
                pinnedShortcutCallbackIntent, /* flags */ 0
            )

            shortcutManager.requestPinShortcut(
                pinShortcutInfo,
                successCallback.intentSender
            )
             Toast.makeText(
                context, "Sound pinned to desktop",
                Toast.LENGTH_LONG
            ).show()
        }
    }

}