package com.acecorp.kaamelottsb.services

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.content.res.AssetFileDescriptor
import java.io.File
import java.io.FileDescriptor
import java.io.FileInputStream

class PlayQuoteService : IntentService("PlayQuoteService") {
    companion object {
        fun publishIntent(context: Context, fileName: String) {
            val i = Intent(context, PlayQuoteService::class.java)
            i.putExtra("file", fileName)
            i.action = "PlayQuoteService"
            context.startService(i)
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        intent?:return
        val fileName = intent.extras?.get("file") as String
        QuotePlayer.playMedia(fileName)
    }


}