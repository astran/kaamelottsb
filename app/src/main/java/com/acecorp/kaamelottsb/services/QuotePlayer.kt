package com.acecorp.kaamelottsb.services

import android.media.MediaPlayer

object QuotePlayer {
    var lastMediaPlayer: MediaPlayer? = null
    var lastPlayingFile: String = ""
    fun playMedia(fileName: String) {

        if (lastMediaPlayer != null && lastMediaPlayer!!.isPlaying) {

            lastMediaPlayer!!.stop()
            if (lastPlayingFile == fileName) {
                return
            }
        }


        val mediaPlayer = MediaPlayer()

        try {
            mediaPlayer.setDataSource(fileName)
            mediaPlayer.prepare()
            mediaPlayer.start()
            lastMediaPlayer = mediaPlayer
            lastPlayingFile = fileName
        } catch (e: Exception) {
//            Toast.makeText(
//                this, "File not found ${fileName}",
//                Toast.LENGTH_LONG
//            ).show()
            e.printStackTrace()
        }
    }
}