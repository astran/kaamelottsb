package com.acecorp.kaamelottsb.services

import android.util.Log
import com.acecorp.kaamelottsb.Quote
import com.acecorp.kaamelottsb.QuoteCategory
import com.google.gson.Gson
import java.io.File
import java.io.FileInputStream
import java.io.FileWriter
import java.io.InputStreamReader

object StarsService {
    var content = mutableMapOf<QuoteCategory, MutableSet<String>>()
    var starsFile: File? = null

    var initialized = false
    fun init(dataDirectory: File) {
        if (!initialized) {
            starsFile = File(dataDirectory, "stars.json")
            val loadStarsFromFile = loadStarsFromFile()
            content = loadStarsFromFile
            initialized = true
        }
    }

    private fun loadStarsFromFile(): MutableMap<QuoteCategory, MutableSet<String>> {

        if (starsFile != null && starsFile?.exists() == true) {
            val gson = Gson()
            val inputStreamReader = InputStreamReader(FileInputStream(starsFile))
            val file = gson.fromJson(inputStreamReader, StaredData::class.java)
            inputStreamReader.close()
            return file.stars
        }
        return mutableMapOf()
    }

    private fun writeStarsFromFile() {
        if (starsFile != null) {
            val gson = Gson()
            val fileWriter = FileWriter(starsFile)
            gson.toJson(StaredData(content), fileWriter)
            fileWriter.close()
        }

    }

    fun isStarred(quote: Quote): Boolean {
        return content[quote.quoteCategory]?.contains(quote.file) ?: false
    }

    fun flipStar(quote: Quote) {
        if (isStarred(quote)) {
            content[quote.quoteCategory]?.remove(quote.file)
        } else {
            if (!content.containsKey(quote.quoteCategory)) {
                content[quote.quoteCategory!!] = mutableSetOf()
            }
            content[quote.quoteCategory]?.add(quote.file)
        }
        writeStarsFromFile()
    }


    data class StaredData(val stars: MutableMap<QuoteCategory, MutableSet<String>>)
}