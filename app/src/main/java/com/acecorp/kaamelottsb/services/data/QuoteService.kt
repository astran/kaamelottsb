package com.acecorp.kaamelottsb.services.data

import android.content.Context
import com.acecorp.kaamelottsb.Quote
import com.acecorp.kaamelottsb.QuoteCategory
import com.acecorp.kaamelottsb.services.StarsService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.reflect.Type


object QuoteService {
    var quotesMap: MutableMap<QuoteCategory, List<Quote>> = mutableMapOf()

    fun filterAsync(searchString: String = "", quoteCategory: QuoteCategory, filtered: (List<Quote>) -> Unit) {

        var filteredQuotes = getQuotesOfCategory(quoteCategory)
        filteredQuotes = if (searchString.isEmpty()) {
            filteredQuotes
        } else {
            filteredQuotes.filter { it.containsString(searchString) }
        }

        val sortedWith = filteredQuotes.sortedWith(
            compareBy(StarsService::isStarred).reversed()
                .thenComparing(compareBy(Quote::quoteCategory, Quote::searchDescription))
        )
        filtered(sortedWith)
    }

    private fun getQuotesOfCategory(quoteCategory: QuoteCategory) =
        if (quoteCategory == QuoteCategory.ALL) {
            quotesMap.values.flatten()
        } else {
            quotesMap[quoteCategory].orEmpty()
        }

    fun loadAllQuotes(context: Context, forceDownload: Boolean = false, callbacks: (List<Quote>) -> Unit) {
        loadDiv(
            context,
            forceDownload
        ) { divQuotes ->
            loadKaamelott(context, forceDownload) { kaamelottQuotes ->
                callbacks(divQuotes + kaamelottQuotes)
            }

        }
    }

    fun loadDiv(context: Context, forceDownload: Boolean = false, callbacks: (List<Quote>) -> Unit) {
        val quoteCategory = QuoteCategory.DIV
        if (!forceDownload
            && !quotesMap[quoteCategory].isNullOrEmpty()
        ) {
            callbacks(quotesMap[quoteCategory].orEmpty())
        } else {
            QuotesLoader.loadDiv(context, forceDownload, updateQuotes(quoteCategory, callbacks))
        }


    }

    fun loadKaamelott(context: Context, forceDownload: Boolean = false, callbacks: (List<Quote>) -> Unit) {
        val quoteCategory = QuoteCategory.KAAMELOTT
        if (!forceDownload && !quotesMap[quoteCategory].isNullOrEmpty()) {
            callbacks(quotesMap[quoteCategory].orEmpty())
        } else {
            QuotesLoader.loadKaamelott(context, forceDownload, updateQuotes(quoteCategory, callbacks))
        }

    }


    fun updateQuotes(quoteCategory: QuoteCategory, callbacks: (List<Quote>) -> Unit): (List<Quote>) -> Unit {
        return { quotes ->
            quotesMap[quoteCategory] = quotes
            callbacks(quotes)
        }
    }
}