package com.acecorp.kaamelottsb.services.data

import android.content.Context
import com.acecorp.kaamelottsb.ProgressValue
import com.acecorp.kaamelottsb.services.DownloadTask
import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

object QuoteDownloader {
    fun reload(
        tempDir: File,
        fileName: String,
        url: String,
        targetDir: File, progressBar: ProgressValue, callBack: () -> Unit
    ) {
        tempDir.mkdirs()
        downloadFile(File(tempDir, fileName), url, progressBar) {
            unpackZip(it.absolutePath, targetDir.absolutePath)
            it.delete()
            callBack()
        }

    }

    fun reload(fileName: String, url: String, context: Context, progressBar: ProgressValue, callBack: () -> Unit) {
        reload(
            context.cacheDir,
            fileName,
            url,
            File(context.filesDir, "sounds"),
            progressBar,
            callBack
        )
    }

    private fun downloadFile(
        targetFile: File,
        url: String,
        progressBar: ProgressValue,
        callBack: (file: File) -> Unit
    ) {

        val downloadTask = DownloadTask(targetFile, progressBar, callBack)
        downloadTask.execute(url)

    }

    private fun unpackZip(zipFile: String, unzipDir: String): Boolean {
        val inputStream: InputStream
        val zis: ZipInputStream
        try {
            var filename: String
            inputStream = FileInputStream(File(zipFile))
            zis = ZipInputStream(BufferedInputStream(inputStream))
            var ze: ZipEntry?
            val buffer = ByteArray(1024)
            var count: Int

            while (true) {
                ze = zis.nextEntry
                if (ze == null) {
                    break
                }
                filename = ze.name

                // Need to create directories if not exists, or
                // it will generate an Exception...
                val file = File(File(unzipDir).absolutePath + "/" + filename)
                file.parentFile.mkdirs()
                if (ze.isDirectory) {
                    file.mkdirs()
                    continue
                }
                val fout = FileOutputStream(file)
                while (zis.read(buffer).also { count = it } != -1) {
                    fout.write(buffer, 0, count)
                }
                fout.close()
                zis.closeEntry()
            }
            zis.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        return true
    }
}