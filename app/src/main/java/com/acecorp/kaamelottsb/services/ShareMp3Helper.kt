package com.acecorp.kaamelottsb.services

import android.content.Context
import android.content.Intent
import android.support.v4.content.FileProvider
import com.acecorp.kaamelottsb.Quote

object ShareMp3Helper {
    fun shareMp3(context: Context, item: Quote) {
        val contentUri = FileProvider.getUriForFile(context, "com.acecorp.kaamelottsb", item.fileRef)
        val share = Intent(Intent.ACTION_SEND)
        share.type = "audio/*"
        share.putExtra(Intent.EXTRA_SUBJECT, item.file)
        share.putExtra(Intent.EXTRA_TEXT, "")
        share.putExtra(Intent.EXTRA_STREAM, contentUri)
        context.startActivity(Intent.createChooser(share, "Share Sound File"))
    }


}