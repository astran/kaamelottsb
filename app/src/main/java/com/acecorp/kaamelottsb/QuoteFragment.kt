package com.acecorp.kaamelottsb

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acecorp.kaamelottsb.services.data.QuoteService


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [QuoteFragment.OnListFragmentInteractionListener] interface.
 */
class QuoteFragment : Fragment() {


    private var listener: OnListQuoteFragmentInteractionListner? = null
    private var quoteType: QuoteCategory = QuoteCategory.ALL
    private var myQuoteRecyclerViewAdapter: MyQuoteRecyclerViewAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            quoteType = QuoteCategory.valueOf(it.getString(LOAD_TYPE)!!)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_quote_list, container, false)
        if (view is RecyclerView) {
            with(view) {
                myQuoteRecyclerViewAdapter = createQuoteRecyclerViewAdapter()
                adapter = myQuoteRecyclerViewAdapter

            }
        }
        return view
    }

    fun filter(s: CharSequence) {
        myQuoteRecyclerViewAdapter?.filter?.filter(s)
    }

    private fun RecyclerView.createQuoteRecyclerViewAdapter(): MyQuoteRecyclerViewAdapter {
        val myQuoteRecyclerViewAdapter =
            MyQuoteRecyclerViewAdapter(quoteType, listener)


        quoteType.quoteLoaderFunction(context, false) {
            QuoteService.filterAsync(quoteCategory = quoteType){
                myQuoteRecyclerViewAdapter.updateData(it)
            }

        }
        return myQuoteRecyclerViewAdapter
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListQuoteFragmentInteractionListner) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */


    companion object {

        const val LOAD_TYPE = "LOAD_TYPE"

        @JvmStatic
        fun newInstance(quoteCategory: QuoteCategory = QuoteCategory.ALL) =
            QuoteFragment().apply {
                arguments = Bundle().apply {
                    putString(LOAD_TYPE, quoteCategory.name)
                }
            }
    }
}
