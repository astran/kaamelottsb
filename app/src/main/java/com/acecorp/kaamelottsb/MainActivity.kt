package com.acecorp.kaamelottsb

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import com.acecorp.kaamelottsb.services.PlayQuoteService
import com.acecorp.kaamelottsb.services.data.QuoteService
import com.acecorp.kaamelottsb.services.data.QuotesLoader
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnListQuoteFragmentInteractionListner, ActivityWithProgressBar {
    override fun onListFragmentInteraction(item: Quote?) {
        PlayQuoteService.publishIntent(this, item!!.fileRef.absolutePath)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu?.findItem(R.id.quote_search)
        val searchText = searchItem?.actionView as SearchView
        searchText.imeOptions = EditorInfo.IME_ACTION_DONE
        searchText!!.isIconified = false
        val listView: View? = this.findViewById(R.id.fragments_list)
        val fragment = listView as RecyclerView
        val adapter = fragment.adapter as MyQuoteRecyclerViewAdapter
        searchText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })

        return true
    }

    override fun progressValue(): ProgressValue {
        return ProgressBarValue(progressBar)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.reload_quotes -> {
                reloadQuotes()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun reloadQuotes() {
        QuoteService.loadAllQuotes(this, true) {

        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}

