package com.acecorp.kaamelottsb


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.acecorp.kaamelottsb.services.ActorsIcons
import com.acecorp.kaamelottsb.services.CreatePinnedShortcutHelper
import com.acecorp.kaamelottsb.services.ShareMp3Helper
import com.acecorp.kaamelottsb.services.StarsService
import com.acecorp.kaamelottsb.services.data.QuoteService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_quote.view.*


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class MyQuoteRecyclerViewAdapter(
    private val quoteCategory: QuoteCategory,
    private val mListener: OnListQuoteFragmentInteractionListner?
) : RecyclerView.Adapter<MyQuoteRecyclerViewAdapter.ViewHolder>(), Filterable {
    var filteredQuotes = listOf<Quote>()

    val roundedCorners = RequestOptions().transforms(CenterCrop(), RoundedCorners(90))


    private val mOnClickListener: View.OnClickListener

    init {

        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Quote
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    fun updateData(quotes: List<Quote>) {
        filteredQuotes = quotes
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()

                val filterResults = FilterResults()
                QuoteService.filterAsync(charString, quoteCategory) {
                    filterResults.count = it.size
                    filterResults.values = it
                }


                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                filteredQuotes = filterResults.values as List<Quote>
                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_quote, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = filteredQuotes[position]

        holder.setContent(item)
        with(holder.clickablePart()) {
            this?.tag = item
            this?.setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = filteredQuotes.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val actorText: TextView = mView.quote_actor
        val titleText: TextView = mView.quote_title
        private val thumbnail: ImageView = mView.thumbnail
        private val createShortcutButton = mView.pin_quote_to_desktop
        private val sharebutton = mView.share_quote

        fun setImage(imageResource: Int) {
            Glide.with(mView).load(imageResource).apply(roundedCorners).into(thumbnail)
        }

        fun clickablePart(): ViewGroup? {
            return mView.quote_clickable_part
        }

        fun setContent(item: Quote) {
            actorText.text = item.character
            titleText.text = item.title
            setImage(ActorsIcons.retrieveActorIcon(item.character))
            setCreateShortcutButton(item)
            setShareButton(item)
        }

        private fun setShareButton(item: Quote) {
            sharebutton.setOnClickListener {
                ShareMp3Helper.shareMp3(sharebutton.context, item)
            }
        }

        private fun setCreateShortcutButton(item: Quote) {
            if (StarsService.isStarred(item)) {
                createShortcutButton.setImageResource(R.drawable.ic_star_true)
            } else {
                createShortcutButton.setImageResource(R.drawable.ic_star_false)
            }

            createShortcutButton.setOnClickListener {
                StarsService.flipStar(item)
                notifyDataSetChanged()
                //CreatePinnedShortcutHelper.pinShortcut(createShortcutButton.context, item)
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + titleText.text + "'"
        }
    }
}
