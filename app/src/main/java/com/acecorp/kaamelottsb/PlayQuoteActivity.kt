package com.acecorp.kaamelottsb

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.acecorp.kaamelottsb.services.PlayQuoteService


class PlayQuoteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fileName = intent.extras.get("file") as String
        PlayQuoteService.publishIntent(this, fileName)
        moveTaskToBack(true)
        finish()
    }
}
