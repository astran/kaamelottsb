package com.acecorp.kaamelottsb

import android.view.View
import android.widget.ProgressBar

interface ProgressValue {
    fun progress(value: Int)
    fun hide()
    fun reset()
}
class EmptyProgessValue() : ProgressValue {
    override fun progress(value: Int) {
    }

    override fun hide() {
    }

    override fun reset() {
    }

}
class ProgressBarValue(private val progressBar: ProgressBar?) : ProgressValue {
    override fun progress(value: Int) {
        if(progressBar?.visibility ==View.GONE){
            progressBar?.visibility = View.VISIBLE
        }
        progressBar?.setProgress(value, true)
    }

    override fun hide() {
        progressBar?.visibility = View.GONE
    }

    override fun reset() {
        progressBar?.progress = 0
        progressBar?.visibility = View.VISIBLE
    }
}