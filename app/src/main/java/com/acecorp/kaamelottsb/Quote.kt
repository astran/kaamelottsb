package com.acecorp.kaamelottsb

import java.io.File
import java.text.Normalizer

data class Quote(
    val character: String,
    val episode: String,
    val file: String,
    val title: String,
    val fileRef: File,
    val quoteCategory: QuoteCategory?
) {
    val searchDescription = normalize("${this.character} ${this.title}")
    fun containsString(s: String): Boolean {
        val searchString = normalize(s)
        return searchDescription.contains(
            searchString
        )

    }

    private fun normalize(s: String) =
        Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).replace(Regex("[\\p{M}.,;:']"), "")
}